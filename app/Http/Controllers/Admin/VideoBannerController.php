<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoBannerRequest;

use Acme\Facades\Activity;

use App\VideoBanner;
use App\Seo;

class VideoBannerController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($frametag = $request->frametag) {
            $data = VideoBanner::where('frametag', 'LIKE', '%' . $frametag . '%')->paginate(25);
        } else {
            $data = VideoBanner::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/video_banners/index')
            ->with('title', 'VideoBanners')
            ->with('menu', 'video_banners')
            ->with('keyword', $request->frametag)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/video_banners/create')
            ->with('title', 'Create video_banner')
            ->with('menu', 'video_banners');
    }
    
    public function store(VideoBannerRequest $request)
    {
        $input = $request->all();
        $video_banner = VideoBanner::create($input);
        
        // $log = 'creates a new video_banner "' . $video_banner->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminVideoBannersEdit', [$video_banner->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/video_banners/show')
            ->with('title', 'Show video_banner')
            ->with('data', VideoBanner::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/video_banners/view')
            ->with('title', 'View video_banner')
            ->with('menu', 'video_banners')
            ->with('data', VideoBanner::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = VideoBanner::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/video_banners/edit')
            ->with('title', 'Edit video_banner')
            ->with('menu', 'video_banners')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(VideoBannerRequest $request, $id)
    {
        $input = $request->all();
        $video_banner = VideoBanner::findOrFail($id);
        $video_banner->update($input);

        // $log = 'edits a video_banner "' . $video_banner->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = VideoBanner::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = VideoBanner::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->frametag;
        }
        // $log = 'deletes a new video_banner "' . implode(', ', $names) . '"';
        // Activity::create($log);

        VideoBanner::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminVideoBanners')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/video_banners', array('as'=>'adminVideoBanners','uses'=>'Admin\VideoBannerController@index'));
Route::get('admin/video_banners/create', array('as'=>'adminVideoBannersCreate','uses'=>'Admin\VideoBannerController@create'));
Route::post('admin/video_banners/', array('as'=>'adminVideoBannersStore','uses'=>'Admin\VideoBannerController@store'));
Route::get('admin/video_banners/{id}/show', array('as'=>'adminVideoBannersShow','uses'=>'Admin\VideoBannerController@show'));
Route::get('admin/video_banners/{id}/view', array('as'=>'adminVideoBannersView','uses'=>'Admin\VideoBannerController@view'));
Route::get('admin/video_banners/{id}/edit', array('as'=>'adminVideoBannersEdit','uses'=>'Admin\VideoBannerController@edit'));
Route::patch('admin/video_banners/{id}', array('as'=>'adminVideoBannersUpdate','uses'=>'Admin\VideoBannerController@update'));
Route::post('admin/video_banners/seo', array('as'=>'adminVideoBannersSeo','uses'=>'Admin\VideoBannerController@seo'));
Route::delete('admin/video_banners/destroy', array('as'=>'adminVideoBannersDestroy','uses'=>'Admin\VideoBannerController@destroy'));
*/
}
