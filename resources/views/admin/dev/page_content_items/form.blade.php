<input type="hidden" name="content_id" value="{{ $content->id }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="row">
    <div class="col-sm-8">
		@if($content->allow_add_items !== 1)
        <div class="caboodle-card no-padding">
            <div class="caboodle-card-header no-padding margin-sides-20">
                <div class="pad-left-10 pad-right flex align-center">
                    <div class="animated-placeholder flex-1 {{@$data->name ? 'filled' : ''}}">
                        <input type="text" name="item_name" value="{{@$data->name}}" class="bundle-name-input" required>
                        <div class="placeholder">
                            <span class="add-on-empty">Enter c</span><span class="add-on-filled">C</span>ontent name <span class="add-on-empty">here...</span>
                        </div>
                    </div>
                    <div class="flex-auto text-center">
                        <div>
                            <label class="x-small" for="published">
                                Enabled
                            </label>
                        </div>
                        <div class="mdc-switch no-margin">
                            <input
                                type="checkbox"
                                id="basic-switch"
                                class="mdc-switch__native-control"
                                name="enabled"
                                {{ (@$data->enabled == 1 || !@$data )? 'checked' : '' }}
                            />
                            <div class="mdc-switch__background">
                                <div class="mdc-switch__knob"></div>
                            </div>
                        </div>
					</div>
                </div>
            </div>
            <div class="caboodle-card-body pad-left-30 pad-right-30">
                <div class="caboodle-form-group">
                    <label for="price">Slug</label>
                    {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
				</div>
                <div class="caboodle-form-group">
					<label for="published">Published</label>
					{!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
				</div>
				<div class="caboodle-form-group">
					<div>
						<label class="x-small" for="published">
							Editable
						</label>
					</div>
					<div class="mdc-switch no-margin">
						<input type="checkbox" id="basic-switch" class="mdc-switch__native-control" name="editable"
							{{ (@$data->editable == 1 || !@$data )? 'checked' : '' }} />
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
            </div>
		</div>
		@endif
		@if(isset($data))
			@include('admin.dev.page_controls.card')
		@endif
    </div>
</div>
