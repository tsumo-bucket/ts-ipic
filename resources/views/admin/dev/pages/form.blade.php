<div class="row">
    <div class="col-sm-{{isset($data) ? '7' : '12'}}">
        <div class="caboodle-card no-padding">
			<div class="caboodle-card-header no-padding margin-sides-20">
                <div class="pad-left-10 pad-right flex align-center">
                    <div class="animated-placeholder flex-1 {{@$data->name ? 'filled' : ''}}">
                        <input type="text" name="name" value="{{@$data->name}}" class="bundle-name-input" required>
                        <div class="placeholder">
                            <span class="add-on-empty">Enter p</span><span class="add-on-filled">P</span>age name <span class="add-on-empty">here...</span>
                        </div>
                    </div>

					{{--<div class="flex-auto text-center">
                        <div>
                            <label class="x-small" for="published">
                                Published
                            </label>
                        </div>
                        <div class="mdc-switch no-margin">
						{!! Form::checkbox('published', '1', @$page->published == 'published', ['class'=>'mdc-switch__native-control', 'data-id' => @$page->id]) !!}
                            <div class="mdc-switch__background">
                                <div class="mdc-switch__knob"></div>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
            <div class="caboodle-card-body pad-left-30 pad-right-30">
                <div class="caboodle-form-group">
                    <label for="slug">Slug</label>
                    {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'', 'autocomplete'=>'off', 'required']) !!}
				</div>

				<div class="caboodle-form-group">
					<div>
						<label class="x-small" for="allow_add_contents">
							Freely add page contents
						</label>
					</div>
					<div class="mdc-switch no-margin">
						<input 
						type="checkbox" 
						id="basic-switch" 
						class="mdc-switch__native-control" name="allow_add_contents"
						 {{ (@$data->allow_add_contents == 1)? 'checked' : '' }} />
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
            </div>
		</div>
	</div>
	@if(isset($data))
		<div class="col-sm-5">
			<div class="caboodle-card">
				<div class="caboodle-card-header pad-top-15 pad-bottom-15">
					<div class="filters no-padding">
						<div class="flex align-center caboodle-form-control-connected">
							<h4 class="no-margin flex-1">
								Page Contents
							</h4>
							@if ($data->allow_add_contents !== 1)
							<a href="{{route('adminPageContentsCreate', $data->id)}}" class="btn-transparent btn-sm flex-1 text-right">
								<i class="far fa-plus-circle"></i> Add
							</a>
							@endif
						</div>
					</div>
				</div>
				<div class="caboodle-card-body">
					@if ($data->allow_add_contents !== 1)
					<a href="{{route('adminPageContentsCreate', $data->id)}}">
						<div class="empty-message {{$data->pageContent->count() > 0 ? 'hide' : ''}}" role="button">
							Add a page content here...
						</div>
					</a>
					@else
					<a href="{{route('adminPageContentsEdit', $content->id)}}">
						<div class="empty-message" role="button">
							Set/Modify page contents form controls here...
						</div>
					</a>
					@endif
                    <table width="100%">
						<tbody id="sortable" sortable-data-url="{{route('adminPageContentsOrder')}}">
						@if ($data->allow_add_contents !== 1)
							@foreach($data->pageContent as $content)
								<tr sortable-id="page_contents-{{$content->id}}">
									<td>
										<div class="flex align-center margin-bottom">
										<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i>
											<div class="flex-1 no-margin">
												@if ($content->name !== '' && $content->name !== null)
												<strong>{{$content->name}}</strong>
												@else
												<span class="sub-text-2 uppercase">Unnamed page content</span>
												@endif
												<input type="hidden" name="page_content[]" value="{{$content->id}}">
											</div>
											<div class="flex-auto">
												<a href="{{route('adminPageContentsEdit', $content->id)}}">
													<i class="far fa-edit hover-scale color-primary" role="button" data-toggle="tooltip" title="Edit">
													</i>
												</a>
												&nbsp;
												<a href="{{ route('adminPageContentsDestroy', $content->id) }}" class="item-delete" data-id="{{ $content->id }}">
													<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
												</a>
											</div>
										</div>
									</td>
								</tr>
							@endforeach
						@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	@endif
</div>


@section('added-scripts')
@include('admin.pages.partials.added-script-ordering')
<script>
   $('.item-delete').on('click', function(e) {
		e.preventDefault();
		var link = $(this).attr('href');
		var id = $(this).data('id');
		swal({
			title: "Delete item?",
			text: 'This action cannot be reversed after saving.',
			type: "warning",
			html: true,
			showCancelButton: true,
			confirmButtonColor: "#dc4f34",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel",
		}, function (isConfirm) {
			if (isConfirm) {
				showLoader(true);
				showNotifyToaster('info', '', '<i class="far fa-circle-notch fa-spin" style="margin-right: 5px"></i> Deleting item');
				$.ajax({
					type: "DELETE",
					url: link,
					data: { 
						_token: "{{ csrf_token() }}",
						id: id
					},
					success: function (data) {
						var title = data.notifTitle;
						var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
						var status = (_.isUndefined(data.notifStatus) ? '' : data.notifStatus);

						if (status != '' && (title != '' || message != '')) {
							toastr.clear();
							showNotifyToaster(status, title, message);
						}
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					},
					error: function (data, text, error) {
						showLoader(false);
						var message = '';
						_.each(data.responseJSON, function (val) {
							message += val + ' ';
						});
						toastr.clear();
						showNotifyToaster('error', 'Error deleting.', message);
					}
				});
			}
		});
	});
<?php
	// STORE UNISELECTOR
	// var storeSelector = $('.stores-uniselector').uniSelector({
    //     dataCategory: 'Store', //-- This will serve as the data name for the uni-selector
    //     title: 'Select Store', //-- A custom title header for the uni-selector
    //     confirmButtonText: 'Select', //-- A custom text for the confirm button of the uni-selector
    //     disableSelected: true,
    //     filters: [
    //         {
    //             "name": "name",
    //             "type": "text",
    //             "placeholder": "Store name",
    //             "width": "100%",
    //             "icon": "far fa-search",
    //             "query": {
    //                 "method": "where",
    //                 "logic": "LIKE",
    //                 "wildcard": "end",
    //                 "relationship": {
    //                     "model": "customer",
    //                     "method": "where",
    //                     "field": "fullname",
    //                     "method": "where",
    //                     "logic": "LIKE",
    //                     "wildcard": "end"
    //                 }
    //             }
    //         }
    //     ],
    //     apiURL: "{{route('adminCaboodleUniSelectorStores')}}", //-- A route for the API endpoint where the data will be fetched,
    //     selectedDataContainer: "#selectedStores", //-- An ID or a class of an element(s) that will serve as a container of the selected items
    //     emptyMessage: "Connect this page control to a store." //-- An empty message for the selectedDataContainer
    // });
    
    // storeSelector.onConfirm(function (selected, addedItems) {
    //     confirmStoreSelection(selected);
    // });

    // function confirmStoreSelection(selected) {
    //     $("form").find("[name='store_ids[]']").remove();
    //     _.each(selected, function(item) {
    //         // !NOTE -- item.key is derived from the id value
	// 		$("form").append(`<input type="hidden" name="store_ids[]" value="${item.key}" />`);
	// 	});
	// }
	?>
</script>
@endsection