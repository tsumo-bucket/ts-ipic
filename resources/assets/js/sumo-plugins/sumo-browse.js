 /*!
  * Sumo Browse
  * http://sumofy.me/
  *
  * Copyright Think Sumo Creative Media Inc.
  * Developed by Jiez Lawsin
  * Released under the MIT license.
  * http://sumofy.me/
  * v1.2.2
  *
  * NOTE: SINGLE INITIALIZATIONS ONLY
  * NOTE: MAXIMUM ARRAY DEPTH: 1 
  */

(function ($) {

    $.fn.sumobrowse = function (options) {
        if (typeof _.each === 'undefined') {
            throw new Error('sumo-browse requires Underscore.js to be loaded first');
        }
        
        if (typeof Handlebars === 'undefined') {
            throw new Error('sumo-browse requires Handlebars.js to be loaded first');
        }

        var defaults = {
            multiple: true, // bool | multiple select for values, returns array, if false, returns object
            title: 'Browse', // string | header title
            searchPlaceholder: 'Search', // string | search placeholder text
            disableSearch: false,
            confirmButtonText: 'Select', // string | default/override confirm button text 
            cancelButtonText: 'Cancel', // string | default/override cancel button text
            data: [], // array | default/override array of objects. fields required: { values |object|, key |string|integer|, text |string|, html |string|, children |array| } 
            disableSelected: false, // bool | default/override disabling selected checkboxes 
            searchInput: null, // element | set element for input event watch,
			columnHeader: 'Details', // string | custom column header
			typingInterval: 500, // number | default typing interval for each key down
        };
        
        // Extend those options
		options = $.extend(defaults, options);
		
		//Initialize typingTimer
		var typingTimer;
        
        // Initialize modal
        var _selected = [];
        var contentTemplate;
        var _tableData;
        
        /* Initialize templates */
        var elID = sumoGUID();
        var modalTemplate = Handlebars.compile($("#sumo-browse-modal-template").html());
        $('#sumo-browse-modals').append(modalTemplate({ id: elID, multiple: options.multiple }));

        var modal = $('#sumo-browse-' + elID);
        modal.on('shown.bs.modal', function() {
            modal.find('.modal-header-title').html(options.title);
            modal.find('.modal-confirm-button').html(options.confirmButtonText);
            modal.find('.modal-cancel-button').html(options.cancelButtonText);
            modal.find('.modal-column-header').html(options.columnHeader);
            try {
                modal.find('.sumo-browse-search-input').val(options.searchInput.val());
            } catch(e) {}
        });

        var filterTemplate = Handlebars.compile($("#sumo-browse-filter-template").html());
        
        if (options.multiple) {
            contentTemplate = Handlebars.compile($("#sumo-browse-content-multiple-template").html());
        } else {
            contentTemplate = Handlebars.compile($("#sumo-browse-content-single-template").html());
        }
        
        if (!options.disableSearch) {
            modal.find('.modal-filters').html(filterTemplate({ placeholder: options.searchPlaceholder }));
        }
        
        updateTableData(options.data);
        /* Initialize templates -- END */
        
        /* TARGET IS CLICKED EVENT FUNCTION 
         * @event click
         */
        $(this).on('click', function(e) {
            showModal(true);
        });

        /* ON SEARCH INPUT KEY UP AND KEY DOWN FILTER DATA TABLE 
         * @event keyup
         */
        modal.find('.sumo-browse-search-input').on('keyup', function() {
			// if (options.searchInput != null) options.searchInput.val(keyword);
            // onSearchKeyup($(this).val());
		});
		        
        /* SHOW/HIDE BROWSE MODAL FUNCTION 
         * @params status = |bool|required| action parameter to show/hide modal
         */
        function showModal(status) {
            var status = (status) ? 'show' : 'hide';
            modal.modal(status);
            initSelectedDataToTable(_tableData);
        }

        function initSelectedDataToTable(data) {
            var selectedParent = [];
            if (options.multiple) {
                _.map(data, function (val, key) {
                    var parentDepth = modal.find('.depth-parent[key="' + val.key + '"]');
                    var parentMDC = parentDepth.find('.mdc-checkbox');
                    var parentCheckbox = parentDepth.find('[type="checkbox"]');
                    var parentMDCCheckbox = new MDCCheckbox(parentMDC[0]);
                    parentCheckbox.attr('disabled', false);
                    parentMDC.toggleClass('disabled', false);

                    if (!_.isUndefined(val.children) && val.children.length > 0) {
                        var selectedChildren = [];
                        _.map(val.children, function (child, key) {
                            var isSelected = _.findWhere(_selected, { key: child.key });
                            var childDepth = modal.find('.depth-child[key="' + child.key + '"]');
                            var childMDC = childDepth.find('.mdc-checkbox');
                            var childCheckbox = childDepth.find('[type="checkbox"]');
                            childCheckbox.attr('disabled', false);
                            childMDC.toggleClass('disabled', false);
                            if (!_.isUndefined(isSelected)) {
                                selectedChildren.push(isSelected);
                                childCheckbox.prop('checked', true);
                                if (options.disableSelected) {
                                    childCheckbox.attr('disabled', true);
                                    childMDC.toggleClass('disabled', true);
                                }
                            } else {
                                childCheckbox.prop('checked', false);
                            }
                        });

                        if (selectedChildren.length == val.children.length) {
                            selectedParent.push(val);
                            parentCheckbox.prop('checked', true);
                            parentMDCCheckbox.checked = true;
                            parentMDCCheckbox.indeterminate = false;
                            if (options.disableSelected) {
                                parentCheckbox.attr('disabled', true);
                                parentMDC.addClass('disabled');
                            }
                        } else {
                            parentCheckbox.prop('checked', false);
                            if (selectedChildren.length > 0) {
                                parentMDCCheckbox.indeterminate = true;
                                parentMDCCheckbox.checked = false;
                            } else {
                                parentMDCCheckbox.checked = false;
                                parentMDCCheckbox.indeterminate = false;
                            }
                        }
                    } else {
                        var isSelected = _.findWhere(_selected, { key: val.key });
                        if (!_.isUndefined(isSelected)) {
                            selectedParent.push(isSelected);
                            parentCheckbox.prop('checked', true);
                            if (options.disableSelected) {
                                parentCheckbox.attr('disabled', true);
                                parentMDC.addClass('disabled');
                            }
                        } else {
                            parentCheckbox.prop('checked', false);
                        }
                    }
                });

                var select_all_checkbox = modal.find('[name="select_all"]');
                var select_all_MDC = select_all_checkbox.closest('.mdc-checkbox');
                var select_all_MDCCheckbox = new MDCCheckbox(select_all_MDC[0]);
                if (selectedParent.length == data.length) {
                    select_all_checkbox.prop('checked', true);
                    select_all_MDCCheckbox.checked = true;
                    select_all_MDCCheckbox.indeterminate = false;
                } else {
                    select_all_checkbox.prop('checked', false);
                    if (selectedParent.length > 0) {
                        select_all_MDCCheckbox.indeterminate = true;
                        select_all_MDCCheckbox.checked = false;
                    } else {
                        select_all_MDCCheckbox.checked = false;
                        select_all_MDCCheckbox.indeterminate = false;
                    }
                }
            } else {
                if (_selected.length > 0) {
                    modal.find('[data-key="' + _selected[0].key + '"]').prop("checked", true);
                }
            }
        }

        /* UPDATE TABLE DATA FUNCTION 
         * @params data = |array|required| fields required: { values, key, text, html, children }
         */
        function updateTableData(data) {
            _tableData = data;

            _.map(data, function (val, key) {
                val.parentDepth = 0;
                if (!_.isUndefined(val.children)) {
                    if (val.children.length > 0) {
                        val.parentDepth = 1;
                        _.map(val.children, function (child, key) {
                            child.parent = val.key;
                            child.serialized = JSON.stringify({
                                key: child.key,
                                text: child.text,
                                values: child.values,
                                parent: child.parent
                            });
                        });
                    }
                }
                val.serialized = JSON.stringify({
                    key: val.key,
                    text: val.text,
                    values: val.values,
                    parentDepth: val.parentDepth
                });
            });
            modal.find('.modal-body-content tbody').html(contentTemplate(data));
            initSelectedDataToTable(data);
        }

        /* UPDATE TABLE DATA FUNCTION 
         * @params selected = |array|object|null| fields required: { key }; overrides (global) selected
         */
        function updateSelected(usrSelected) {
            _selected = usrSelected;
        }

        /* ON SEARCH INPUT KEYUP 
         * @params keyword = |string|null| filter table data by keyword
         */
        function onSearchKeyup(keyword) {
            // console.log('onSearchKeyup', keyword);
		}
		
        return {
            /* SHOW BROWSE MODAL 
             * @params keyword = |string|null| default search keyword value on show
             * @params selected = |array|object|null| fields required: { key }; overrides options.data
             */
            show: function (keyword, usrSelected) {
                var keyword = (_.isUndefined(keyword) ? false : keyword);
                var usrSelected = (_.isUndefined(usrSelected) ? false : usrSelected);
                
                if (keyword) {
                    console.log(keyword);
                    modal.find('.sumo-browse-search-input').val(keyword).focus();
                }
                showModal(true);
            },
            /* HIDE BROWSE MODAL */
            hide: function() {
                showModal(false);
            },
            /* SHOW/HIDE LOADER 
             * @params status = |bool|required| action parameter to show/hide loader 
             */
            loader: function(status) {
                if (status) {
                    modal.find('.browse-modal').toggleClass('hide', false);
                    modal.find('.modal-footer').find('[type="button"]').attr('disabled', true);
                } else {
                    modal.find('.browse-modal').toggleClass('hide', true);
                    modal.find('.modal-footer').find('[type="button"]').attr('disabled', false);
                }
            },
            /* SEARCH INPUT ON KEYUP CALLBACK 
             * @params callback = |func|required| called when keyup event is triggered from jQuery
             */
            onSearch: function(callback) {
                function actionCall() {
                    callback(modal.find('.sumo-browse-search-input').val());
				}
				modal.find('.sumo-browse-search-input').on('keyup', function () {
					clearTimeout(typingTimer);
					typingTimer = setTimeout(actionCall, options.typingInterval);
				});
				modal.find('.sumo-browse-search-input').on('keydown', function () {
					clearTimeout(typingTimer);
				});
                // modal.find('.sumo-browse-search-input').on('keyup', actionCall);
            },
            /* UPDATE MODAL DATA (will update table data) 
             * @params data = |array|required| fields required: { values, key, text, html, children }; ovverides options.data
             * @params selected = |array|object|null| fields required: { key }; overrides options.data
             */
            updateData: function(data, usrSelected) {
                var usrSelected = (_.isUndefined(usrSelected) ? false : usrSelected);
                options.data = data;
                
                if (usrSelected) {
                    _selected = usrSelected;
                }

                updateTableData(options.data);
            },
            /* UPDATE OPTIONS (will update options data) 
             * @params newOptions = |object|null| extends to options
             */
            updateOptions: function(newOptions) {
                // Extend those new options
                options = $.extend(options, newOptions);
            },
            /* ON CONFIRM BUTTON CLICK CALLBACK 
             * @params callback = |func|required| called when keyup event is triggered from jQuery
             */
            onConfirm: function(callback) {
                modal.find('.modal-confirm-button').on('click', function (e) {
                    var form = modal.find('form');
                    var serialized = form.serializeArray();
                    var tmpSelected = [];
                    var _currentSelected = [];
                    _.each(serialized, function (val) {
                        if (val.name != 'select_all') {
                            try {
                                val = JSON.parse(val.value);
                                tmpSelected.push(val);
                            } catch (e) { }
                        }
                    });
                    _currentSelected = tmpSelected.slice();
                    if (options.multiple) {
                        if (tmpSelected.length) {
                            _.each(tmpSelected, function (val) {
                                var isSelected = _.findWhere(_selected, { key: val.key });
                                if (isSelected) isSelected.value = val.value;
                                else _selected.push(val);
                            });
                        } else {
                            if (!options.disableSelected) _selected = tmpSelected.slice();
                        }
                    } else {
                        _selected = tmpSelected.slice();
                    }
                    
                    /* callback 
                     * @params _selected = overall selected including previous selected
                     * @params _currentSelected = only newly selected items
                     */
                    callback(_selected, _currentSelected);
                });
            },
            onShow: function(callback) {
                function actionCall() {
                    callback();
                }
                modal.on('shown.bs.modal', actionCall);
            },
            beforeShow: function(callback) {
                function actionCall() {
                    callback();
                }
                modal.on('show.bs.modal', actionCall);
            },
            modal: modal,
            target: $(this)
        };
    }

    /* CUSTOMIZED HANDLE BARS HELPERS */
    /* HANDLEBARS HELPER: LENGTH CONDITION RETURNS boolean
    * @param data |array|required array to check length
    */
    Handlebars.registerHelper('length', function (arr, options) {
        return (arr.length > 0) ? options.fn(this) : options.inverse(this);
    });
    Handlebars.registerHelper('elseLength', function (arr, options) {
        return (arr.length == 0) ? options.fn(this) : options.inverse(this);
    });

    /* HANDLEBARS HELPER: HIDE CLASS FROM BOOLEAN 
    * @param bool |boolean|required boolean to parse
    */
    Handlebars.registerHelper('hide', function (bool) {
       return (bool) ? 'hide' : '';
    });

    /* HANDLEBARS HELPER: SET PROP CHECKED FROM BOOLEAN 
    * @param bool |booleanarray|required boolean to parse
    */
    Handlebars.registerHelper('check', function (bool) {
        return (bool) ? 'checked' : '';
    });
    /* CUSTOMIZED HANDLE BARS HELPERS -- END */

    function sumoGUID() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
})(jQuery);